## Open Source projects @ Innovation garage AS

For our main website, see https://www.innovationgarage.no/

## Completed projects

### [ProtoSwitchBoard](https://innovationgarage.github.io/ProtoSwitchBoard)
Does all your electronics prototypes end up looking like a snake nest? ProtoSwitchBoard is a labeled interconnect for your project designed to make your experimental prototypes as easy to follow as your circuit diagram!

### [TFPrism](https://innovationgarage.github.io/tfprism/)
TFPrism is a library that transforms your tensorflow graph to automatically do data parallelism for training. All you need to do to modify your single-cpu tensorflow code to run on a cluster is to send your training op and feed_dict through the library.

### [label-V](https://innovationgarage.github.io/label-V)
A semi-automatic video labeling tool. Label-V uses the multi-tracking algorithm in OpenCV to generate training data for your computer vision project.

### [Scramble](https://github.com/innovationgarage/Scramble)
Firefox scramble plugin for scrambing web pages. Nice to use for scrambling dashboards, monitoring data etc. where you don't want to accidentally leak customer or other private data.

## Prototypes / alpha versions

### [Cloudberry](https://innovationgarage.github.io/cloudberry/)
Documentation of, and a set of extensions on top of OpenWRT, OpenWISP and OpenVPN to implement network-to-docker-container-to-network routed vpn that supports both end-points being behind NAT and DHCP.

### [Price Tag Reader](https://github.com/innovationgarage/price-tag-reader)
Project to read supermarket price tags from photos to increase price transparency in the food market.

### [ChalkMail](https://github.com/innovationgarage/ChalkMail)
Write your email address on a white-board/table top at the end of a meeting to have a snapshot of the board sent to your inbox.

